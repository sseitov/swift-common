//
//  UIViewControllerExtension.swift
//
//  Created by Сергей Сейтов on 22.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit

extension UINavigationController {
    
    open override var childViewControllerForStatusBarStyle: UIViewController? {
        return self.topViewController
    }
    
    open override var childViewControllerForStatusBarHidden: UIViewController? {
        return self.topViewController
    }
}

extension UIViewController {
    
    func setupTitle(_ text:String, color:UIColor = .white) {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: navigationController!.navigationBar.frame.height))
        label.textAlignment = .center
        label.font = UIFont.condensed(15)
        label.text = text
        label.textColor = color
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        navigationItem.titleView = label
    }
    
    func setupBackButton() {
        navigationItem.leftBarButtonItem?.target = self
        navigationItem.leftBarButtonItem?.action = #selector(UIViewController.goBack)
    }
    
    @objc func goBack() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func topBarHeight() -> CGFloat {
        if #available(iOS 11.0, *) {
            return view.safeAreaInsets.top
        } else {
            let statusHeight:CGFloat = UIApplication.shared.statusBarOrientation.isPortrait ? 20 : 0
            return navigationController!.navigationBar.frame.height + statusHeight
        }
    }
}
