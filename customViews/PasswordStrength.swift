//
//  PasswordStrength.swift
//  cryptoBox (MilCryptor Secure Platform)
//
//  Created by Denys Borysiuk on 19.07.16.
//  Copyright © 2016 ArchiSec Solutions, Ltd. All rights reserved.//
//

import UIKit

enum PasswordStrengthAlign {
    case top
    case bottom
}

class PasswordStrength: UIView {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        valueView = UIView(frame: self.bounds)
        valueView.setupBorder(UIColor.clear, radius: 4)
        self.addSubview(valueView)
        var rc = self.bounds
        rc.origin.y -= 20
        rc.size.height = 20
        textView = UILabel(frame: rc)
        textView.font = UIFont(name: "HelveticaNeue", size: 12)
        textView.textColor = UIColor.white
        textView.textAlignment = .center
        self.addSubview(textView)
    }
    
    var align:PasswordStrengthAlign = .top {
        didSet {
            var rc = self.bounds
            if align == .top {
                rc.origin.y -= 20
            } else {
                rc.origin.y = rc.height
            }
            rc.size.height = 20
            textView.frame = rc
            setNeedsLayout()
        }
    }
    
    var valueView:UIView!
    var textView:UILabel!
    
    var strength:NJOPasswordStrength? {
        didSet {
            if strength != nil {
                self.isHidden = false
                textView.text = NJOPasswordStrengthEvaluator.localizedString(for: strength!)

                let value = strength!.rawValue
                let step = self.bounds.width/5.0
                let w = CGFloat(value+1)*step
                let rc = CGRect(x: (self.bounds.width - w)/2.0, y: 0, width: w, height: self.bounds.height)
                let path = CGPath(rect: rc, transform: nil)
                let maskLayer = CAShapeLayer()
                maskLayer.path = path
                valueView.layer.mask = maskLayer

                switch value {
                case 0:
                    valueView.backgroundColor = UIColor.red
                case 1:
                    valueView.backgroundColor = UIColor.yellow
                case 2:
                    valueView.backgroundColor = UIColor.magenta
                case 3:
                    valueView.backgroundColor = UIColor.green
                default:
                    valueView.backgroundColor = UIColor.blue
                }
                
                self.setNeedsDisplay()
            } else {
                self.isHidden = true
            }
        }
    }
}
