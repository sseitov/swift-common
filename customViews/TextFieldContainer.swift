//
//  TextFieldContainer.swift
//
//  Created by Сергей Сейтов on 19.07.16.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit

protocol TextFieldContainerDelegate:class {
    func textDone(_ sender:TextFieldContainer, text:String?)
    func textChange(_ sender:TextFieldContainer, text:String?) -> Bool
}

class TextFieldContainer: UIView, UITextFieldDelegate {

    weak var delegate:TextFieldContainerDelegate?
    
    var nonActiveColor:UIColor = .main
    var activeColor:UIColor = .mainDark
    var placeholderColor = UIColor.mainBorder
    
    var placeholder: String = "" {
        didSet {
            textField.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [NSAttributedStringKey.foregroundColor : placeholderColor])
        }
    }
    
    var isSecure: Bool = false {
        didSet {
            textField.isSecureTextEntry = isSecure
            if isSecure, textField.rightView == nil {
                let eye = UIButton(type: .custom)
                eye.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
                eye.setImage(UIImage(named: "eye"), for: .normal)
                eye.addTarget(self, action: #selector(self.tapEye), for: .touchUpInside)
                textField.rightViewMode = .always
                textField.rightView = eye
                textField.rightView?.isHidden = true
            }
        }
    }
    
    var textField:UITextField!
    
    @objc
    func tapEye() {
        textField.isSecureTextEntry = !textField.isSecureTextEntry
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textField = UITextField(frame: self.bounds.insetBy(dx: 0, dy: 3))
        textField.textAlignment = .center
        textField.autocorrectionType = .no
        textField.autocapitalizationType = .none
        textField.tintColor = UIColor.white
        textField.delegate = self
        textField.font = UIFont(name: "SFUIDisplay-Regular", size: 15)

        if #available(iOS 11, *) {
            // Disables the password autoFill accessory view.
            textField.textContentType = UITextContentType("")
        }
 
        configure()

        self.addSubview(textField)
    }
    
    class func deactivateAll() {
        UIApplication.shared.sendAction(#selector(UIApplication.resignFirstResponder), to: nil, from: nil, for: nil)
    }
    
    class func inputError(error:String, responder:TextFieldContainer) {
        Alert.message(title: "Input Error".uppercased(), message: error, okHandler: {
            responder.activate(true)
        })
    }
    
    func configure() {
        backgroundColor = UIColor.clear
        textField.textColor = UIColor.white
        textField.textAlignment = .left
        placeholderColor = UIColor.lightGray
        textField.borderStyle = .none
        nonActiveColor = UIColor.clear
        activeColor = UIColor.clear
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        textField.frame = self.bounds.insetBy(dx: 0, dy: 3)
    }
    
    func activate(_ active:Bool) {
        if active {
            textField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
    }
    
    func text() -> String {
        return textField.text == nil ? "" : textField.text!
    }
    
    func setText(_ text:String) {
        textField.text = text
        if text.isEmpty {
            textField.rightView?.isHidden = true
        }
    }
    
    func clear() {
        textField.text = ""
        textField.rightView?.isHidden = true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newText:String? = textField.text != nil ? (textField.text! as NSString).replacingCharacters(in: range, with: string) : nil
        if newText == nil || newText!.isEmpty {
            textField.rightView?.isHidden = true
            backgroundColor = nonActiveColor
        } else {
            textField.rightView?.isHidden = false
            backgroundColor = activeColor
        }
        if string == "\n" {
            textField.resignFirstResponder()
            delegate?.textDone(self, text: textField.text)
            return false
        } else {
            if delegate != nil {
                return delegate!.textChange(self, text: newText)
            } else {
                return true
            }
        }
    }
}
